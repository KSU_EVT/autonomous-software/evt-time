#include <stdint.h>

// This library assumes a *single thread* with the capability to do 32-bit atomic reads and writes.
// evt_pps_round() can be called in an ISR

struct evt_time_t {
    int32_t seconds = 0; // secs since Unix epoch, negative values allowed
    uint32_t microseconds = 0; // microsecs since last sec; [0, 1e6)
};

// force set the current time
// NOT INTERRUPT SAFE; call this before attaching an interrupt for PPS
void evt_set_time(evt_time_t time);

// round to the nearest second - should be called in an ISR when a PPS signal is received
void evt_pps_round();

// get the current time
evt_time_t evt_time_now();
