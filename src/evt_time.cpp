#include "evt_time.hpp"

#include "Arduino.h"

struct _evt_global_time_t {
    evt_time_t time;
    uint32_t last_update_micros = 0;
};

static volatile uint32_t last_isr_write;
static volatile _evt_global_time_t global_time_blue;
static volatile _evt_global_time_t global_time_green;
static volatile _evt_global_time_t* current_global_time = &global_time_blue;

void evt_set_time(evt_time_t time) {
    uint32_t cur_us = micros();

    global_time_blue.time = time;
    global_time_blue.last_update_micros = cur_us;
    global_time_green.time = time;
    global_time_green.last_update_micros = cur_us;
}

// micros() can be somewhat dangerous can to use in an ISR, but it's our very first line of code so we
// should be good. The only potential danger is if a *ton* of different ISRs with higher priority take
// over before micros() returns.
void evt_pps_round() {
    // debounce; necessary for our assumption that current_global_time won't be flipped more than once
    // in between reads of the 96 bit struct it points to
    uint32_t cur_us = micros();
    if (cur_us - last_isr_write < 1000) return;

    evt_time_t curr = evt_time_now();
    int32_t secs = curr.seconds;
    uint32_t usecs = curr.microseconds;

    secs += usecs / 1000000;
    usecs %= 1000000;

    // round up
    if (usecs >= 500000) {
        secs += 1;
    }
    usecs = 0;

    // BEGIN CRITICAL SECTION
    if (current_global_time == &global_time_blue) {
        global_time_green.last_update_micros = cur_us;
        last_isr_write = cur_us;

        global_time_green.time.seconds = secs;
        global_time_green.time.microseconds = usecs;

        current_global_time = &global_time_green;
    } else {
        global_time_blue.last_update_micros = cur_us;
        last_isr_write = cur_us;

        global_time_blue.time.seconds = secs;
        global_time_blue.time.microseconds = usecs;

        current_global_time = &global_time_blue;
    }
    // END CRITICAL SECTION
}

evt_time_t evt_time_now() {
    // BEGIN CRITICAL SECTION
    _evt_global_time_t* time_snapshot_ptr = current_global_time;
    // END CRITICAL SECTION

    // We make the assumption that current_global_time won't change more than once during the dereference below
    // (of a 96 bit struct).
    _evt_global_time_t time_snapshot = *time_snapshot_ptr;

    // both are uint32_t so the diff will always be absolute value
    uint32_t us_diff = micros() - time_snapshot.last_update_micros; 
    time_snapshot.time.microseconds += us_diff;
    time_snapshot.time.seconds += time_snapshot.time.microseconds / 1000000;
    time_snapshot.time.microseconds %= 1000000;

    return time_snapshot.time;
}
